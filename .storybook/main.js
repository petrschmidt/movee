module.exports = {
  stories: ['../packages/**/*.stories.ts', '../packages/**/*.stories.tsx'],
  addons: ['@storybook/preset-create-react-app', '@storybook/addon-docs']
};
